import time
import itertools as it


def timing(f):
    def wrap(*args):
        time1 = time.time()
        ret = f(*args)
        time2 = time.time()
        print '%s function took %0.6f us' % (f.func_name, (time2 - time1) * 1000000)
        return ret

    return wrap


def gen_seq():
    a, b = 0, 1
    while True:
        yield a
        b = a+b
        yield b
        a = a+b


@timing
def fib_mod(n, m):
    """
    :return: compute Fn modulo m using matrix form
    https://en.wikipedia.org/wiki/Fibonacci_number#Matrix_form
    """
    # Initialize a matrix [[1,1],[1,0]]
    v1, v2, v3 = 1, 1, 0
    # Perform fast exponentiation of the matrix (quickly raise it to the nth power)
    for rec in bin(n)[3:]:
        calc = (v2 * v2) % m
        v1, v2, v3 = (v1 * v1 + calc) % m, ((v1 + v3) * v2) % m, (calc + v3 * v3) % m
        if rec == '1': v1, v2, v3 = (v1 + v2) % m, v1, v2
    print(v2);

@timing
def seq(f, len):
    for x in it.islice(f, len):
        print(x)


def main():
    n, m = map(int, raw_input("Enter N and M:").split());
    fib_mod(n, m)


if __name__ == '__main__':
    main()
